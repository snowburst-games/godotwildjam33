// Audio data: contains references to all audio files in the project for convenience
// Contains method for loading required sounds - should load all sounds required for a stage at the start.
using System;
using System.Collections.Generic;
using Godot;

public static class AudioData
{

	public enum SoundBus {
		Voice,
		Effects,
		Music,
		Master
	}

	public static Dictionary<SoundBus, string> SoundBusStrings = new Dictionary<SoundBus, string>()
	{
		{SoundBus.Voice, "Voice"},
		{SoundBus.Effects, "Effects"},
		{SoundBus.Music, "Music"},
		{SoundBus.Master, "Master"}
	};

	public enum MainMenuSound {
		Music,
		IntroNarration
	};

	public enum ButtonSound {
		Hover,
		Click
	};

	public enum WorldSound {
		Music,
		Defeat,
		Victory,
		Armageddon,
		EndLevel1Narration,
		EndLevel2Narration,
		EndLevel3Narration,
		EndLevel4Narration
	};

	public enum PlayerSound {
		EvilPortalGen,
		EvilPortalTravel1,
		EvilPortalTravel2,
		PlayerWalk,
		SwitchPortalGun,
		ObstaclePush,
		Fizzle
	}

	public enum ObstaclePushSound {
		Pushed
	}

	public enum NiceCreatureSound {
		NiceFound1,
		FoundNice2,
		Ow,
		Ooh,
		Aah,
		Yay,
		Woohoo1,
		Freedom
	}

	public enum GoodPortalSound
	{
		Escape,
		Fade,
		Gen
	}

	public static Dictionary<GoodPortalSound, string> GoodPortalSoundPaths = new Dictionary<GoodPortalSound, string>()
	{
		{GoodPortalSound.Escape, "res://Props/NicePortal/GoodPortalEscape.wav"},
		{GoodPortalSound.Fade, "res://Props/NicePortal/GoodPortalFade.wav"},
		{GoodPortalSound.Gen, "res://Props/NicePortal/GoodPortalGen.wav"},
	};


	public static Dictionary<ObstaclePushSound, string> ObstaclePushSoundPaths = new Dictionary<ObstaclePushSound, string>()
	{
		{ObstaclePushSound.Pushed, "res://Actors/Player/Sound/ObstaclePush.wav"}
	};

	public static Dictionary<NiceCreatureSound, string> NiceCreatureSoundPaths = new Dictionary<NiceCreatureSound, string>()
	{
		{NiceCreatureSound.NiceFound1, "res://Actors/NiceCreature/Sounds/NiceFound1.wav"},
		{NiceCreatureSound.FoundNice2, "res://Actors/NiceCreature/Sounds/FoundNice2.wav"},
		{NiceCreatureSound.Freedom, "res://Actors/NiceCreature/Sounds/Freedom.wav"},
		{NiceCreatureSound.Ow, "res://Actors/NiceCreature/Sounds/Ow.wav"},
		{NiceCreatureSound.Ooh, "res://Actors/NiceCreature/Sounds/Ooh.wav"},
		{NiceCreatureSound.Aah, "res://Actors/NiceCreature/Sounds/Aah.wav"},
		{NiceCreatureSound.Woohoo1, "res://Actors/NiceCreature/Sounds/Woohoo1.wav"},
		{NiceCreatureSound.Yay, "res://Actors/NiceCreature/Sounds/Yay.wav"},
	};


	public static Dictionary<PlayerSound, string> PlayerSoundPaths = new Dictionary<PlayerSound, string>()
	{
		{PlayerSound.EvilPortalGen, "res://Actors/Player/Sound/EvilPortalGen.wav"},
		{PlayerSound.EvilPortalTravel1, "res://Actors/Player/Sound/EvilPortalTravel1.wav"},
		{PlayerSound.EvilPortalTravel2, "res://Actors/Player/Sound/EvilPortalTravel2.wav"},
		{PlayerSound.PlayerWalk, "res://Actors/Player/Sound/PlayerWalk.wav"},
		{PlayerSound.SwitchPortalGun, "res://Actors/Player/Sound/SwitchPortalGun.wav"},
		{PlayerSound.Fizzle, "res://Actors/Player/Sound/Fizzle.wav"}
	};

	public static Dictionary<MainMenuSound, string> MainMenuSoundPaths = new Dictionary<MainMenuSound, string>()
	{
		{MainMenuSound.Music, "res://Stages/MainMenu/Music/CandyCantBeBuy-Menu.mp3"},
		{MainMenuSound.IntroNarration, "res://Stages/MainMenu/TodayIsGoingToBe.wav"}
	};

	public static Dictionary<WorldSound, string> WorldSoundPaths = new Dictionary<WorldSound, string>()
	{
		{WorldSound.Music, "res://Stages/World/Music/DeeperIntoCandyland-World.mp3"},
		{WorldSound.Victory, "res://Stages/World/Sound/Victory.wav"},
		{WorldSound.Defeat, "res://Stages/World/Sound/Defeat.wav"},
		{WorldSound.Armageddon, "res://Stages/World/Sound/Armageddon.wav"},
		{WorldSound.EndLevel1Narration, "res://Stages/World/Narration/Level1.wav"},
		{WorldSound.EndLevel2Narration, "res://Stages/World/Narration/Level2.wav"},
		{WorldSound.EndLevel3Narration, "res://Stages/World/Narration/Level3.wav"},
		{WorldSound.EndLevel4Narration, "res://Stages/World/Narration/Level4.wav"}
	};


	public static Dictionary<ButtonSound, string> ButtonSoundPaths = new Dictionary<ButtonSound, string>()
	{
		{ButtonSound.Click, "res://Interface/Buttons/BtnBase/ButtonClickScifi.wav"},
		{ButtonSound.Hover, "res://Interface/Buttons/BtnBase/ButtonHoverSciFi.wav"},
	};

	// Return a dictionary containing loaded sounds. Useful to do when loading/instancing a scene e.g. player.
	// Usage: Dictionary<AudioData.PlayerSounds, AudioStream> playerSounds = 
	//  AudioData.LoadedSounds<AudioData.PlayerSounds>(AudioData.PlayerSoundPaths);
	// If 2D positional sound is required then add a AudioStreamPlayer2D node as a child and:
	// AudioStreamPlayer2D soundPlayer = (AudioStreamPlayer2D) GetNode("SoundPlayer");
	// // Play sound
	// AudioHandler.PlaySound(soundPlayer, playerSounds[AudioData.PlayerSounds.Test], AudioData.SoundBus.Effects);
	// Change to AudioStreamPlayer for music/global sounds and AudioStreamPlayer3D if working in 3D
	public static Dictionary<T, AudioStream> LoadedSounds<T>(Dictionary<T, string> soundPathsDict)
	{
		Dictionary<T, AudioStream> loadedSoundsDict = new Dictionary<T, AudioStream>();
		foreach (T key in soundPathsDict.Keys)
		{
			loadedSoundsDict[key] = (AudioStream) GD.Load(soundPathsDict[key]);
		}
		return loadedSoundsDict;
	}


}
