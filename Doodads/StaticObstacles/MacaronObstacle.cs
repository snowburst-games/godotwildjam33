using Godot;
using System;
using System.Collections.Generic;

public class MacaronObstacle : StaticBody2D
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";
	private Sprite _sprite;
	private Random _rand = new Random();
	private List<Texture> _textures = new List<Texture>() {
		GD.Load<Texture>("res://Doodads/StaticObstacles/Art/blue macaron v0.4..png"),
		GD.Load<Texture>("res://Doodads/StaticObstacles/Art/gold macaron v0.3.png"),
		GD.Load<Texture>("res://Doodads/StaticObstacles/Art/green macaron v0.2.png"),
		GD.Load<Texture>("res://Doodads/StaticObstacles/Art/purple macaron v0.3.png")
	};

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		_sprite = GetNode<Sprite>("Sprite");
		_sprite.Texture = _textures[_rand.Next(0, _textures.Count)];		
	}

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
}
