using Godot;
using System;

public class PlayerTest : KinematicBody2D
{
	private float _power = 50;
	public override void _PhysicsProcess(float delta)
	{
		base._PhysicsProcess(delta);

		Vector2 direction = new Vector2();

		if (Input.IsActionPressed("Up"))
		{
			direction += new Vector2(0,-1);
		}
		if (Input.IsActionPressed("Right"))
		{
			direction += new Vector2(1,0);
		}
		if (Input.IsActionPressed("Down"))
		{
			direction += new Vector2(0,1);
		}
		if (Input.IsActionPressed("Left"))
		{
			direction += new Vector2(-1,0);
		}

		MoveAndSlide(direction.Normalized() * 300, infiniteInertia:false);

		for(int i = 0; i < GetSlideCount(); i++)
		{
			var collision = GetSlideCollision(i);
			if (collision.Collider is PushableObstacle obstacle)
			{
				obstacle.OnPushed(-collision.Normal * _power);		
			}
		}
	}
}
// for index in get_slide_count():
//         var collision = get_slide_collision(index)
//             if collision.collider.is_in_group("bodies"):
//                 collision.collider.apply_central_impulse(-collision.normal * push)
