using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

public class CreatureGeneratorManager : YSort
{
	[Export]
	private int _numberOfGenerators = 3;
	public override void _Ready()
	{
		List<CreatureGenerator> creaturegens = new List<CreatureGenerator>();
		foreach (Node n in GetChildren())
		{
			if (n is CreatureGenerator creatureGenerator)
			{
				creaturegens.Add(creatureGenerator);
			}
		}

		Random rand = new Random();
		creaturegens = creaturegens.ToList().OrderBy(a => rand.Next()).ToList();
		for (int i = 0; i < creaturegens.Count - _numberOfGenerators; i++)
		{
			CreatureGenerator toRemove = creaturegens[0];
			toRemove.QueueFree();
			creaturegens.Remove(toRemove);			
		}
	}

	public void KillAllGeneratorCreatures()
	{
		foreach (Node n in GetChildren())
		{
			if (n is CreatureGenerator generator)
			{
				generator.KillCreatures();
			}
		}
	}

}
