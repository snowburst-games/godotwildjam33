using Godot;
using System;
using System.Collections.Generic;

public class StageWorld : Stage
{
	private AudioStreamPlayer _musicPlayer;
	private AudioStreamPlayer _soundPlayer;
	private AudioStreamPlayer _endSoundsPlayer;

	public enum LevelWorld { Level1, Level2, Level3, Level4, Level5, End}

	private LevelWorld _currentLevel = LevelWorld.Level1;

	private Level _currentLevelScene;

	public Dictionary<LevelWorld, PackedScene> _levels = new Dictionary<LevelWorld, PackedScene>()
	{
		{LevelWorld.Level1, GD.Load<PackedScene>("res://Levels/Level1.tscn")},
		{LevelWorld.Level2, GD.Load<PackedScene>("res://Levels/Level2.tscn")},
		{LevelWorld.Level3, GD.Load<PackedScene>("res://Levels/Level3.tscn")},
		{LevelWorld.Level4, GD.Load<PackedScene>("res://Levels/Level4.tscn")},
		{LevelWorld.Level5, GD.Load<PackedScene>("res://Levels/Level5.tscn")}
	};

	private Dictionary<LevelWorld, string> _levelLeaderboards = new Dictionary<LevelWorld, string>()
	{
		{LevelWorld.Level1, "level1"},
		{LevelWorld.Level2, "level2"},
		{LevelWorld.Level3, "level3"},
		{LevelWorld.Level4, "level4"},
		{LevelWorld.Level5, "level5"}
	};

	private AnimationPlayer _HUDAnim;
	private Timer _levelTimer;
	private Label _lblTimeLeft;
	private Label _lblEscaped;
	private Label _lblEvilled;
	private Label _lblTarget;
	private Label _lblTargetEscape;
	private Label _lblLevelName;
	private Label _lblScore;
	private Button _btnPortal1;
	private Button _btnPortal2;
	private Button _btnArmageddon;
	private int _score = 0;
	private Node2D _levelContainer;

	private PortalManager _portalManager1;
	private PortalManager _portalManager2;

	private Player _player;
	private Cauldron _cauldron;
	private PictureStory _pictureStory;

	private int _escapedCount = 0;
	private int _evilledCount = 0;

	// this should be in level as an exported variable
	private int _targetEvil = 20;
	private int _targetEscape = 5;
	//
	private Dictionary<AudioData.WorldSound, AudioStream> _worldSounds = AudioData.LoadedSounds<AudioData.WorldSound>(AudioData.WorldSoundPaths);

	public override void _Ready()
	{
		base._Ready();
		SetProcess(false);

		_HUDAnim = GetNode<AnimationPlayer>("HUD/AnimHUD");
		_lblTimeLeft = GetNode<Label>("HUD/VBoxContainer/HBoxContainer2/LblTimeLeft");
		_lblEscaped = GetNode<Label>("HUD/VBoxContainer/HBoxContainer/LblEscaped");
		_lblEvilled = GetNode<Label>("HUD/VBoxContainer/HBoxContainer/LblEvilled");
		_lblTarget = GetNode<Label>("HUD/VBoxContainer/HBoxContainer2/LblTarget");
		_lblTargetEscape = GetNode<Label>("HUD/VBoxContainer/HBoxContainer2/LblTargetEscape");
		_lblLevelName = GetNode<Label>("HUD/VBoxContainer/LblLevelName");
		_lblScore = GetNode<Label>("HUD/VBoxContainer/HBoxContainer/LblScore");
		_btnPortal1 = GetNode<Button>("HUD/PnlPortalUI/HBoxContainer/BtnPortal1");
		_btnPortal2 = GetNode<Button>("HUD/PnlPortalUI/HBoxContainer/BtnPortal2");
		_btnArmageddon = GetNode<Button>("HUD/BtnArmageddon");
		_pictureStory = GetNode<PictureStory>("HUD/PictureStory");
		_levelContainer = GetNode<Node2D>("LevelContainer");
		_musicPlayer = GetNode<AudioStreamPlayer>("MusicPlayer");
		_soundPlayer = GetNode<AudioStreamPlayer>("SoundPlayer");
		_endSoundsPlayer = GetNode<AudioStreamPlayer>("EndSoundsPlayer");

	}

	private async void OnBtnArmageddonPressed()
	{
		_btnArmageddon.Disabled = true;
		GetNode<CPUParticles2D>("MainExplosion").Position = _player.Position;
		GetNode<AnimationPlayer>("AnimWorld").Play("Armageddon");
		AudioHandler.PlaySound(_soundPlayer, _worldSounds[AudioData.WorldSound.Armageddon], AudioData.SoundBus.Effects);
		EraseEnd();
		await ToSignal(GetNode<AnimationPlayer>("AnimWorld"), "animation_finished");		
		LevelVictory();
	}

	public override void Init()
	{
		base.Init();
		AudioHandler.PlaySound(_musicPlayer, _worldSounds[AudioData.WorldSound.Music], AudioData.SoundBus.Music);

		if (SharedData != null)
		{
			if (SharedData.ContainsKey("Level"))
			{
				if ((StageWorld.LevelWorld)SharedData["Level"] == LevelWorld.End)
				{
					OnFinalVictory();
					return;
				}
				else
				{
					InitiateLevel((StageWorld.LevelWorld)SharedData["Level"]);
					return;
				}
			}
			
		}
		InitiateLevel(LevelWorld.Level1);// the first level goes here
	}

	private void OnLevelCompleted()
	{		
		
		SaveCompletedLevels();
		GetNode<ScoreManagerMono>("HUD/ScoreManagerMono").StartScoreSaver(_score, 10, _levelLeaderboards[_currentLevel]);
		//OnBtnMainMenuPressed();
	}

	private void OnBtnRestartPressed()
	{
		SceneManager.SimpleChangeScene(SceneData.Stage.World, new Dictionary<string, object>() {
			{"Level", _currentLevel}
		});
	}

	public void SaveCompletedLevels()
	{
		LevelData data = new LevelData();
		
		List<int> completedLevels = GetLevelsCompleted();

		if (completedLevels == null)
		{
			completedLevels = new List<int>();
		}
		if (!completedLevels.Contains((int)_currentLevelScene.NextLevel))
		{
			completedLevels.Add((int)_currentLevelScene.NextLevel);
		}
		data.LevelsCompleted = completedLevels;

		FileJSON.SaveToJSON(OS.GetUserDataDir() + "/CompletedLevels.json", data);
	}

	private List<int> GetLevelsCompleted()
	{
		string path = OS.GetUserDataDir() + "/CompletedLevels.json";
		if (! System.IO.File.Exists(path))
		{
			GD.Print("File at " + path + " not found.");
			return null;
		}
		LevelData data = FileJSON.LoadFromJSON<LevelData>(path);
		return data.LevelsCompleted;
	}

	private void OnFinalVictory()
	{
		_pictureStory.Reset();
		_pictureStory.PictureStoryFinished+=this.OnFinalLevelCompleted;
		_pictureStory.StayPausedOnFinish = false;
		_pictureStory.FadeOutOnFinish = false;
		_pictureStory.VisibleOnFinish = true;
		_pictureStory.AddScreen(_currentLevelScene.EndLevelPicturePath, _currentLevelScene.EndLevelText, _worldSounds[_currentLevelScene.AudioPath]);
		_pictureStory.OverrideColor(new Color(0,0,0), new Color(0,0,0));
		_pictureStory.Start();
	}

	private void OnFinalLevelCompleted()
	{
		GetNode<ScoreManagerMono>("HUD/ScoreManagerMono").StartScoreSaver(_evilledCount, 10, _levelLeaderboards[_currentLevel]);
		GetNode<Button>("HUD/ScoreManagerMono/score_manager/leaderboard/btn_close").Visible = true;
		GetNode<Button>("HUD/ScoreManagerMono/score_manager/leaderboard/BtnContinue").Visible = false;
		// OnBtnMainMenuPressed();
	}

	private void OnBtnMainMenuPressed()
	{
		SceneManager.SimpleChangeScene(SceneData.Stage.MainMenu);
	}

	private void DisablePortal()
	{
		_btnPortal1.Disabled = true;
		_btnPortal2.Disabled = true;
		_player.PortalsEnabled = false;
	}

	public void OnPortalGunOneEquipped()
	{
		_btnPortal1.Disabled = true;
		_btnPortal2.Disabled = false;
		_player.EquipPortalGunOne();
	}
	public void OnPortalGunTwoEquipped()
	{
		_btnPortal2.Disabled = true;
		_btnPortal1.Disabled = false;
		_player.EquipPortalGunTwo();
	}

	public void OnPortalGunEquipped(int num)
	{
		if (num == 1)
		{
			OnPortalGunOneEquipped();
			return;
		}
		OnPortalGunTwoEquipped();
	}

	private void InitiateLevel(LevelWorld level)
	{
		_currentLevel = level;
		PackedScene levelScn = _levels[level];
		Level levelToLoad = (Level) levelScn.Instance();
		_levelContainer.AddChild(levelToLoad);
		_currentLevelScene = levelToLoad;
		_targetEvil = levelToLoad.TargetEvil;
		_targetEscape = levelToLoad.EscapeAvoid;


		// set this when new level is made/added
		foreach (Node n in _levelContainer.GetNode("Level/All/CreatureGeneratorManager").GetChildren())
		{
			if (n is CreatureGenerator creatureGen)
			{
				creatureGen.Connect(nameof(CreatureGenerator.CreatureGenerated), this, nameof(OnCreatureGenerated));
			}
		}

		_levelTimer = _levelContainer.GetNode<Timer>("Level/LevelTimer");
		_levelTimer.Connect("timeout", this, nameof(OnLevelTimerTimeout));
		_lblTimeLeft.Text = "Time remaining: " + Math.Ceiling(_levelTimer.WaitTime).ToString();
		_lblTarget.Text = "Target gingerbread to capture: " + _targetEvil;
		_lblTargetEscape.Text = "Lose if " + _targetEscape + " gingerbread escape!";
		_lblLevelName.Text = levelToLoad.LevelName;
		
		_portalManager1 = _levelContainer.GetNode<PortalManager>("Level/All/PortalManager1");
		_portalManager2 = _levelContainer.GetNode<PortalManager>("Level/All/PortalManager2");
		_player = _levelContainer.GetNode<Player>("Level/All/Player");
		_player.Connect(nameof(Player.PlayerHint), this, nameof(HintText));
		_player.Connect(nameof(Player.EquippedPortalGun), this, nameof(OnPortalGunEquipped));
		_cauldron = _levelContainer.GetNode<Cauldron>("Level/All/Cauldron");
		_player.CauldronPos = _cauldron.GlobalPosition;

		// LevelVictory(); // TESTING THE END BITS
		Pregame();
	}

	private async void Pregame()
	{

		// all subject to change when we integrate multiple levels

		// start off paused
		GetTree().Paused = true;
		GetNode<Button>("HUD/BtnMenu").Disabled = true;
		_btnPortal1.Disabled = true;
		_btnPortal2.Disabled = true;

		// play the start animation
		_HUDAnim.Play("Start");

		await ToSignal(_HUDAnim, "animation_finished");
		GetNode<Button>("HUD/BtnMenu").Disabled = false;
		if (_currentLevel == LevelWorld.Level1)
		{
			DisablePortal();
		}
		else
		{
			OnPortalGunOneEquipped();
		}
		GetTree().Paused = false;
		_levelTimer.Start();
		SetProcess(true);
	}

	public override void _Process(float delta)
	{
		base._Process(delta);

		_lblTimeLeft.Text = "Time remaining: " + Math.Ceiling(_levelTimer.TimeLeft).ToString();
		if (_evilledCount >= _targetEvil && _escapedCount < _targetEscape && !_erased)
		{
			_btnArmageddon.Disabled = false;
		}
	}

	public void OnCreatureGenerated(NiceCreature creature)
	{
		creature.Connect(nameof(NiceCreature.Escaped), this, nameof(OnCreatureEscaped));
		creature.Connect(nameof(NiceCreature.Evilled), this, nameof(OnCreatureEvilled));
		// _portalManager1.Connect(nameof(PortalManager.OnTeleport), creature, nameof(NiceCreature.OnTeleport));
		// _portalManager2.Connect(nameof(PortalManager.OnTeleport), creature, nameof(NiceCreature.OnTeleport));
	}

	public void OnCreatureEscaped()
	{
		_escapedCount += 1;
		UpdateScore();
		_lblEscaped.Text = "Gingerbread escaped: " + _escapedCount.ToString();
		if (_escapedCount >= _targetEscape && !_erased)
		{
			LevelDefeat(true);
		}
	}

	public void OnCreatureEvilled()
	{
		_evilledCount += 1;
		UpdateScore();
		_lblEvilled.Text = "Gingerbread captured: " + _evilledCount.ToString();
	}

	private void UpdateScore()
	{
		if (_erased)
		{
			return;
		}
		_score = (_evilledCount*2) - _escapedCount;
		_lblScore.Text = "Score: " + _score;
	}

	public void OnLevelTimerTimeout()
	{
		if (_erased)
		{
			return;
		}
		if (_evilledCount >= _targetEvil && _escapedCount < _targetEscape)
		{
			LevelVictory();
		}
		else
		{
			LevelDefeat(false);
		}
	}
	private bool _erased = false;
	private void EraseEnd()
	{
		if (_erased)
		{
			return;
		}
		_erased = true;
		_currentLevelScene.GetNode<CreatureGeneratorManager>("All/CreatureGeneratorManager").QueueFree();
		_currentLevelScene.GetNode<PortalGen>("All/PortalGen").QueueFree();
		_currentLevelScene.GetNode<Cauldron>("All/Cauldron").QueueFree();
		// _player.QueueFree();
		_player.SetPhysicsProcess(false);
	}

	public void LevelVictory()
	{
		_musicPlayer.VolumeDb = -25;
		_soundPlayer.VolumeDb = -100;
		EraseEnd();
		// _player.SetPhysicsProcess(false);
		AudioHandler.PlaySound(_endSoundsPlayer, _worldSounds[AudioData.WorldSound.Victory], AudioData.SoundBus.Music);
		if (_currentLevelScene.NextLevel == LevelWorld.End)
		{
			OnFinalVictory();
			return;
		}
		_pictureStory.Reset();
		_pictureStory.PictureStoryFinished+=this.OnLevelCompleted;
		_pictureStory.StayPausedOnFinish = false;
		_pictureStory.FadeOutOnFinish = false;
		_pictureStory.VisibleOnFinish = true;
		_pictureStory.AddScreen(_currentLevelScene.EndLevelPicturePath, _currentLevelScene.EndLevelText, _worldSounds[_currentLevelScene.AudioPath]);
		_pictureStory.OverrideColor(new Color(0,0,0), new Color(0,0,0));
		_pictureStory.Start();
		// check scores, update high score
		// GetNode<AnimationPlayer>("AnimWorld").Play("FadeOutLevel");
		// if high score ->
		// then after high score is updated, need to load the picture story

		// go to picture story which then leads to the next level
		// save the level progress
	}

	private void OnBtnContinuePressed()
	{
		SceneManager.SimpleChangeScene(SceneData.Stage.World, new Dictionary<string, object>() {
			{"Level", _currentLevelScene.NextLevel}
		});
	}

	public void LevelDefeat(bool escape)
	{
		_musicPlayer.VolumeDb = -25;
		_soundPlayer.VolumeDb = -100;
		EraseEnd();
		// _player.SetPhysicsProcess(false);
		AudioHandler.PlaySound(_endSoundsPlayer, _worldSounds[AudioData.WorldSound.Defeat], AudioData.SoundBus.Music);
		GetNode<AnimationPlayer>("AnimWorld").Play("FadeOutLevel");
		GetNode<Panel>("HUD/PnlMenu").Visible = true;
		GetNode<Label>("HUD/PnlMenu/LblTitle").Text = "DEFEAT! " + (escape ? "Too many escaped!" : "Out of time!");
		GetNode<Button>("HUD/PnlMenu/VBoxContainer/BtnResume").Disabled = true;
	}


	private void OnScoreSaverNoHighScore()
	{
		// AudioHandler.PlaySound(_soundPlayer, _worldSounds[AudioData.WorldSounds.NoHighScore], AudioData.SoundBus.Music);
	}


	private void OnScoreSaverHighScore()
	{
		// AudioHandler.PlaySound(_soundPlayer, _worldSounds[AudioData.WorldSounds.HighScore], AudioData.SoundBus.Music);
	}


	private void OnScoreSaverFinished()
	{
		GetNode<ScoreManagerMono>("HUD/ScoreManagerMono").OnOptionLevelSelectLevelSelected((int)_currentLevel);
	}

	public void HintText(string text)
	{
		GetNode<Label>("HUD/LblHintText").Text = text;
		_HUDAnim.Play("HintText");
	}
}


