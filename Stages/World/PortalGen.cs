using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

public class PortalGen : YSort
{
	
	private PackedScene _nicePortalScn;
	private List<Vector2> _potentialPortalPositions = new List<Vector2>();
	private List<NicePortal> _nicePortals = new List<NicePortal>();

	[Export]
	private float _portalDuration = 10f;

	[Export]
	private int _maxPortals = 3;

	[Export]
	private int[] _genTimerRange = new int[2] {12, 24};

	private Timer _genTimer;
	private Random _rand = new Random();

	public override void _Ready()
	{
		base._Ready();
		_genTimer = GetNode<Timer>("GenTimer");
		_nicePortalScn = GD.Load<PackedScene>("res://Props/NicePortal/NicePortal.tscn");

		foreach (Node n in GetChildren())
		{
			if (n is Position2D p)
			{
				_potentialPortalPositions.Add(p.Position);
			}
		}

		_maxPortals = Math.Min(_potentialPortalPositions.Count-1, _maxPortals);

		_potentialPortalPositions = _potentialPortalPositions.ToList().OrderBy(a => _rand.Next()).ToList();

		_genTimer.WaitTime = _rand.Next(_genTimerRange[0], _genTimerRange[1]+1);
		if (_maxPortals == 0)
		{
			return;
		}
		_genTimer.Start();
	}

	private void OnGenTimerTimeout()
	{
		if (_maxPortals == 0)
		{
			return;
		}
		Vector2 justRemovedPosition = new Vector2(-99999, -99999);
		if (_nicePortals.Count >= _maxPortals)
		{
			NicePortal removePortal = _nicePortals[0];
			justRemovedPosition = removePortal.Position;
			_nicePortals.Remove(removePortal);
			removePortal.Die();
		}

		NicePortal newPortal = (NicePortal) _nicePortalScn.Instance();
		newPortal.Position = _potentialPortalPositions[_rand.Next(0, _potentialPortalPositions.Count)];
		_potentialPortalPositions.Remove(newPortal.Position);
		_nicePortals.Add(newPortal);
		AddChild(newPortal);
		newPortal.Spawn();

		if (justRemovedPosition != new Vector2(-99999, -99999))
		{
			_potentialPortalPositions.Add(justRemovedPosition);
		}
		_genTimer.WaitTime = _rand.Next(_genTimerRange[0], _genTimerRange[1]+1);
		_genTimer.Start();
	}

}

