Explosion scene. Generate a particles2D explosion of variable size.

Requirements:

Add to csproj for older godot versions:
//  <Compile Include="addons/Effects/Explosion/Explosion.cs" />

Usage:
Generate an explosion:
// Explosion e = Explosion.New();
Add it as child:
// AddChild(e);
Start the explosion in a specified (Vector2) place and of specified (float) size (normal = 1).
// e.Start(pos, size)
E.g:
public void OnDie()
{
    Explosion e = Explosion.New();
    AddChild(e);
    e.Start(Position, 1f);
}
