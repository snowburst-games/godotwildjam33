// // Explosion scene. Generate a particles2D explosion of variable size.
// // Usage example:
// public void OnDie()
// {
//     Explosion e = Explosion.New();
//     AddChild(e);
//     e.Start(Position, 1f);
// }

using Godot;
using System;

public class Explosion : Node2D
{
	private CPUParticles2D _mainExplosion;
	private CPUParticles2D _explosionLayer;
	private CPUParticles2D _smoke;
	private CPUParticles2D _smokeLayer;
	private Timer _deathTimer;

	public override void _Ready()
	{		
		_mainExplosion = GetNode<CPUParticles2D>("MainExplosion");
		_explosionLayer = GetNode<CPUParticles2D>("MainExplosion/ExplosionLayer");
		_smoke = GetNode<CPUParticles2D>("MainExplosion/Smoke");
		_smokeLayer = GetNode<CPUParticles2D>("MainExplosion/SmokeLayer");
		_deathTimer = GetNode<Timer>("DeathTimer");
	}


	public static Explosion New()
	{
		Explosion e = (Explosion)((PackedScene)GD.Load("res://addons/Effects/Explosion/Explosion.tscn")).Instance();
		return e;
	}

	public void Start(Vector2 pos, float scale)
	{
		Scale = new Vector2(1,1) * scale;
		Position = pos;
		_mainExplosion.Emitting = true;
		_explosionLayer.Emitting = true;
		_smoke.Emitting = true;
		_smokeLayer.Emitting = true;
		_deathTimer.Start();
	}

	private void OnDeathTimerTimeout()
	{
		QueueFree();
	}
}


