using Godot;
using System;

public class HUD : CanvasLayer
{
	// Declare member variables here. Examples:
	// private int a = 2;
	// private string b = "text";
	private Panel _pnlMenu;
	private PnlSettings _pnlSettings;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		_pnlMenu = GetNode<Panel>("PnlMenu");
		_pnlSettings = GetNode<PnlSettings>("PnlSettings");
		_pnlMenu.Visible = false;
		_pnlSettings.Visible = false;
		
		if (OS.HasFeature("HTML5"))
		{
			_pnlSettings.GetNode<CheckBox>("CntPanels/PnlGraphics/CBoxFullScreen").Visible = false;
			_pnlSettings.GetNode<Label>("CntPanels/PnlGraphics/LblTitle").Text = "Not available in HTML5 version.";
		}
	}

	public override void _Input(InputEvent ev)
	{
		base._Input(ev);
		if (_pnlSettings.Visible)
		{
			return;
		}
		if (Input.IsActionJustReleased("Menu"))
		{
			if (_pnlMenu.Visible)
			{
				OnBtnResumePressed();
			}
			else
			{
				if (GetTree().Paused)
				{
					return;
				}
				OnBtnMenuPressed();		
			}
		}
        if (ev.IsActionReleased("Pause") && !ev.IsEcho())// .IsActionJustReleased("Pause"))
        {
            OnBtnPausePressed();
        }
	}

    public override void _Process(float delta)
    {
        
    }

	private void OnBtnMenuPressed()
	{	
		_pnlMenu.Visible = true;
		GetTree().Paused = true;
		TogglePanelButtonsDisable(true);
        GetNode<Button>("BtnPause").Disabled = true;
	}

    public void OnBtnPausePressed()
    {
        if (GetNode<AnimationPlayer>("AnimHUD").IsPlaying() && GetNode<AnimationPlayer>("AnimHUD").CurrentAnimation == "Start" ||
            GetNode<Button>("BtnPause").Modulate.a < 1)
        {
            return;
        }
        GetTree().Paused = !GetTree().Paused;
        GetNode<Label>("BtnPause/Lbl||").Visible = !GetTree().Paused;
        GetNode<Label>("BtnPause/Lbl>").Visible = GetTree().Paused;
        TogglePanelButtonsDisable(GetTree().Paused);

    }

	private void OnBtnResumePressed()
	{
		_pnlMenu.Visible = false;
		GetTree().Paused = false;
		TogglePanelButtonsDisable(false);
        GetNode<Button>("BtnPause").Disabled = false;
	}

	private void OnBtnSettingsPressed()
	{
		_pnlSettings.Visible = true;
		// _HUDAnim.Play("ShowSettings");
		// GetTree().Paused = true;
		// TogglePanelButtonsDisable(true);
	}

	private void TogglePanelButtonsDisable(bool disabled)
	{
        GetNode<ColorRect>("PauseRect").Visible = disabled;
		GetNode<Button>("BtnMenu").Disabled = disabled;
	}
	private void OnHUDAnimAnimationFinished(String anim_name)
	{
		// if (anim_name == "ShowMenu")
		// {
		// 	GetTree().Paused = true;
		// }
		// else if (anim_name == "HideMenu")
		// {
		// 	GetTree().Paused = false;
		// }
	}
}
