// Stage menu: simple menu script connecting local and networked game signals to our scene manager.
using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

public class StageMainMenu : Stage
{
	private AudioStreamPlayer _musicPlayer;
	private AudioStreamPlayer _soundPlayer;
	private Dictionary<AudioData.MainMenuSound, AudioStream> _mainMenuSounds = AudioData.LoadedSounds<AudioData.MainMenuSound>(AudioData.MainMenuSoundPaths);
	private PictureStory _pictureStory;
	private Panel _popAbout;
	private PnlSettings _pnlSettings;
	private Dictionary<StageWorld.LevelWorld, Button> _levelButtons;
	private Button _firstLevelButton;

	// If 2D positional sound is required then add a AudioStreamPlayer2D node as a child and:
	// AudioStreamPlayer2D soundPlayer = (AudioStreamPlayer2D) GetNode("SoundPlayer");
	// // Play sound
	// AudioHandler.PlaySound(soundPlayer, playerSounds[AudioData.PlayerSounds.Test], AudioData.SoundBus.Effects);

	public override void _Ready()
	{
		base._Ready();
		// SetBtnToggleFullScreenText();
		GetNode<ScoreManagerMono>("ScoreManagerMono").RefreshLeaderboard(10, "level1");
		_popAbout = GetNode<Panel>("PopAbout");
		_musicPlayer = GetNode<AudioStreamPlayer>("MusicPlayer");
		_soundPlayer = GetNode<AudioStreamPlayer>("SoundPlayer");
		_pictureStory = GetNode<PictureStory>("PictureStory");
		_pnlSettings = GetNode<PnlSettings>("PnlSettings");
		_pnlSettings.Visible = false;

		_levelButtons = new Dictionary<StageWorld.LevelWorld, Button>()
		{
			// {StageWorld.Level.Tutorial, GetNode<Button>("PnlLevels/VBoxContainer/BtnLvlTutorial")},
			{StageWorld.LevelWorld.Level2, GetNode<Button>("PnlLevels/VBoxContainer/BtnLevel2")},
			{StageWorld.LevelWorld.Level3, GetNode<Button>("PnlLevels/VBoxContainer/BtnLevel3")},
			{StageWorld.LevelWorld.Level4, GetNode<Button>("PnlLevels/VBoxContainer/BtnLevel4")},
			//{StageWorld.LevelWorld.Level5, GetNode<Button>("PnlLevels/VBoxContainer/BtnLevel5")},
		};
		_firstLevelButton = GetNode<Button>("PnlLevels/VBoxContainer/BtnLevel1");

		if (OS.HasFeature("HTML5"))
		{
			GetNode<Button>("HBoxContainer/BtnQuit1").Visible = false;
			GetNode<Button>("HBoxContainer/BtnToggleMute").Visible = true;
			_pnlSettings.GetNode<CheckBox>("CntPanels/PnlGraphics/CBoxFullScreen").Visible = false;
			_pnlSettings.GetNode<Label>("CntPanels/PnlGraphics/LblTitle").Text = "Not available in HTML5 version.";
		}
		
		AudioHandler.PlaySound(_musicPlayer, _mainMenuSounds[AudioData.MainMenuSound.Music], AudioData.SoundBus.Music);

		
		_popAbout.Visible = false;
		GetNode<Panel>("PnlLevels").Visible = false;
		GetNode<Button>("HBoxContainer/BtnToggleMute").Text = AudioServer.IsBusMute(AudioServer.GetBusIndex("Master")) ? "UNMUTE" : "MUTE";



		_firstLevelButton.Disabled = false;
		// _firstLevelButton.Connect("pressed", this, nameof(OnLevelButtonPressed), new Godot.Collections.Array() {StageWorld.LevelWorld.Level1});
		_firstLevelButton.Connect("pressed", this, nameof(Intro));


		List<int> levelsCompleted = GetLevelsCompleted();
		if (levelsCompleted != null)
		{
			foreach (StageWorld.LevelWorld l in _levelButtons.Keys)
			{
				if (levelsCompleted.Contains((int)l))
				{
					_levelButtons[l].Disabled = false;
					_levelButtons[l].Connect("pressed", this, nameof(OnLevelButtonPressed), new Godot.Collections.Array() {l});
				}
			}
		}
		// _musicPlayer.PitchScale = 1f;
		// int busEffectsIndex = AudioServer.GetBusIndex(AudioData.SoundBusStrings[AudioData.SoundBus.Music]);
		// int numberofEffects = AudioServer.GetBusEffectCount(busEffectsIndex);
		// for (int i = 0; i < numberofEffects; i++)
		// {
		// 	AudioServer.RemoveBusEffect(busEffectsIndex, i);
		// }
	}

	private void OnLevelButtonPressed(StageWorld.LevelWorld l)
	{
		SceneManager.SimpleChangeScene(SceneData.Stage.World, new Dictionary<string, object>() {
			{"Level", l}
		});
	}

	private List<int> GetLevelsCompleted()
	{
		string path = OS.GetUserDataDir() + "/CompletedLevels.json";
		if (! System.IO.File.Exists(path))
		{
			GD.Print("File at " + path + " not found.");
			return null;
		}
		LevelData data = FileJSON.LoadFromJSON<LevelData>(path);
		return data.LevelsCompleted;
	}

	private void OnBtnPlayPressed()
	{
		Intro();
		
	}

	private void Intro() // this is the new game intro
	{
		_musicPlayer.VolumeDb = -15;
		_pictureStory.Reset();
		_pictureStory.PictureStoryFinished+=this.OnIntroFinished;
		_pictureStory.FadeOutOnFinish = false;
		_pictureStory.VisibleOnFinish = true;
		_pictureStory.AddScreen("res://Stages/MainMenu/PictureStory/PictureStoryIntro.png", 
		@"Today is going to be the best day of Milo’s life. He and his mother have found Candyland! Candyland is full of happy little gingerbread people, the kindest and friendliest of all the sweeties! And the most delicious. Help Milo lure them into Mother's mouth before dinner time.",
		_mainMenuSounds[AudioData.MainMenuSound.IntroNarration]);
		_pictureStory.OverrideColor(new Color(0,0,0), new Color(0,0,0));
		_pictureStory.Start();
	}

	private void OnIntroFinished()
	{
		_musicPlayer.VolumeDb = 0;
		SceneManager.SimpleChangeScene(SceneData.Stage.World);
	}

	private void OnBtnAboutPressed()
	{
		GetNode<Panel>("PopAbout").Visible = true;
		//GetNode<Control>("PopAbout/CntAbout").Visible = true;
	}

	private void OnBtnBackPressed()
	{
		GetNode<Panel>("PopAbout").Visible = false;
	}


	private void OnBtnHideLevelSelectPressed()
	{
		GetNode<Panel>("PnlLevels").Visible = false;
	}

	public override void _Input(InputEvent ev)
	{
		if (_popAbout.Visible && ev is InputEventMouseButton evMouseButton && ev.IsPressed())
		{
			if (! (evMouseButton.Position.x > _popAbout.RectGlobalPosition.x && evMouseButton.Position.x < _popAbout.RectSize.x + _popAbout.RectGlobalPosition.x
			&& evMouseButton.Position.y > _popAbout.RectGlobalPosition.y && evMouseButton.Position.y < _popAbout.RectSize.y + _popAbout.RectGlobalPosition.y) )
			{
				OnBtnBackPressed();
			}
		}
	}

	private void _on_BtnQuit_pressed()
	{
		GetTree().Quit();
	}

	private void ToggleMute()
	{
		bool muted = AudioServer.IsBusMute(AudioServer.GetBusIndex("Master"));
		AudioServer.SetBusMute(AudioServer.GetBusIndex("Master"), !muted);
		string label = muted ? "MUTE" : "UNMUTE";
		GetNode<Button>("HBoxContainer/BtnToggleMute").Text = label;
	}

	private void OnGameLinkButtonPressed()
	{
		OS.ShellOpen("https://sage7.itch.io/");
	}


	private void OnBtnScoresPressed()
	{
		_pnlSettings.Visible = true;
	}

	private void OnBtnTexBackgroundPressed()
	{
		_pnlSettings.OnBtnClosePressed();
		_popAbout.Visible = false;
		GetNode<Panel>("PnlLevels").Visible = false;
	}

	private void OnBtnLeaderboardPressed()
	{
		GetNode<ScoreManagerMono>("ScoreManagerMono").ShowLeaderboard();
	}

	private void OnBtnLevelSelectPressed()
	{
		GetNode<Panel>("PnlLevels").Visible = true;
	}

}
