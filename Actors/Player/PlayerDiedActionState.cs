using Godot;
using System;

public class PlayerDiedActionState : PlayerActionState
{
    public PlayerDiedActionState(Player Player)
    {
        this.Player = Player;
        Player.PlayAnim("Died");
    }
    
    public override void Update(float delta)
    {
        base.Update(delta);
      
    }

    public override void _Process(float delta)
    {
        
    }
}
