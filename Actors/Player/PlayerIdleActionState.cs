using Godot;
using System;

public class PlayerIdleActionState : PlayerActionState
{
    public PlayerIdleActionState(Player Player)
    {
        this.Player = Player;
        Player.PlayAnim("Idle");
    }
    public override void Update(float delta)
    {
        base.Update(delta);
        if (Player.MoveDir.x>0 || Player.MoveDir.x<0 || Player.MoveDir.y>0 || Player.MoveDir.y<0)
        {
            Player.ChangeState(Player.State.Moving);
        }
 /*        if (Input.IsActionJustPressed("set_portal_one"))
        {
           // GD.Print("Set Portal One");
           // Player.ChangeState(Player.State.Portal);
        }
        if (Input.IsActionJustPressed("set_portal_two"))
        {
           // GD.Print("Set Portal Two");
        } */
        if (Player.MoveDir.x == 0)
        {

        }
    }

    public override void _Process(float delta)
    {

    }
}
