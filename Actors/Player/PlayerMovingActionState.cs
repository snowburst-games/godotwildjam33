using Godot;
using System;

public class PlayerMovingActionState : PlayerActionState
{
    public Vector2 DiagonalDownLeft = new Vector2 (-1,1);
	public Vector2 DiagonalUpLeft = new Vector2 (-1,-1);
	public Vector2 DiagonalDownRight = new Vector2 (1,1);
	public Vector2 DiagonalUpRight = new Vector2 (1,-1);
	public Vector2 Down = new Vector2 (0,1);
	public Vector2 Up = new Vector2 (0,-1);
	public Vector2 Right = new Vector2 (1,0);
	public Vector2 Left = new Vector2 (-1,0);
    private bool _stopped = false;

    public PlayerMovingActionState(Player Player)
    {
        this.Player = Player;
        Player.PlayAnim("Right");
    }

    public override void Update(float delta)
    {
        base.Update(delta);
		Player.PlayWalkSound();
        Player.MoveAndSlide(Player.MoveDir.Normalized() * Player.Speed, infiniteInertia:false);
		if (Player.MoveDir == new Vector2 (0,0)) 
		{
			Player.StopPlayerWalkSound();
			Player.ChangeState(Player.State.Idle);
		}
		if ((Player.MoveDir == DiagonalDownRight))
		{
			Player.FlipLeft(false);
			Player.PlayAnim("Down");
			Player.RotationDegree = 90 + 45;	
			Player.Offset = new Vector2(60,50);	  
		}
		if ((Player.MoveDir == DiagonalUpRight))
		{
			Player.FlipLeft(false);
			Player.PlayAnim("Up");
			Player.RotationDegree = 45;	
			Player.Offset = new Vector2(60,-60);
		}
		if ((Player.MoveDir == DiagonalDownLeft))
		{
			Player.FlipLeft(false);
			Player.PlayAnim("Down");
			Player.RotationDegree = 180+45;	
			Player.Offset = new Vector2(-60,50);
		}
		if ((Player.MoveDir == DiagonalUpLeft))
		{
			Player.FlipLeft(false);
			Player.PlayAnim("Up");
			Player.RotationDegree = 270+45;	
			Player.Offset = new Vector2(-60,-60);
		}
		if (Player.MoveDir == Right)
		{
			Player.FlipLeft(false);
			Player.PlayAnim("Right");
			Player.RotationDegree = 90; 
			Player.Offset = new Vector2(60,0);
        }
		if (Player.MoveDir==Left)
		{
			Player.FlipLeft(true);
			Player.PlayAnim("Right");
			Player.RotationDegree = 270;
			Player.Offset = new Vector2(-60,0);
		}
		if (Player.MoveDir == Up)
		{
			Player.FlipLeft(false);
			Player.PlayAnim("Up");
			Player.RotationDegree = 360;
			Player.Offset = new Vector2(0,-80);
		}
		if (Player.MoveDir == Down)
		{
			Player.FlipLeft(false);
			Player.PlayAnim("Down");
			Player.RotationDegree = 180;	
			Player.Offset = new Vector2(0,60);
		}
    }

    public override void _Process(float delta)
    {
        
    }
}
