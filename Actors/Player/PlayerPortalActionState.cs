using Godot;
using System;

public class PlayerPortalActionState : PlayerActionState
{
    public PlayerPortalActionState(Player Player)
    {
        this.Player = Player;
        Player.AimSprite.Visible = true;
    }
    public override void Update(float delta)
    {
        base.Update(delta);
        if (Input.IsActionJustPressed("ui_select"))
        {
        }
        if (Player.MoveDir.x>0 || Player.MoveDir.x<0 || Player.MoveDir.y>0 || Player.MoveDir.y<0)
        {
           // Player.ChangeState(Player.State.Moving);
           // Player.AimSprite.Visible = false;
        }

    }

    public override void _Process(float delta)
    {
        
    }
}
