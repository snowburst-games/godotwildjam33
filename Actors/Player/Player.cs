using Godot;
using System;
using System.Collections.Generic;

public class Player : KinematicBody2D, ITeleportable
{
	private float _pushPower = 100;
	public PlayerActionState PlayerActionState {get; set;}
	public Vector2 MoveDir;
	public float Speed {get; set;} = 300;
	public CollisionShape2D CollisionShape2D;
	public AnimationPlayer Anim;
	public Sprite Sprite;
	public CollisionShape2D DetectArea;
	public int RotationDegree = 0;
	public Vector2 Offset = new Vector2(0,0);
	public Sprite AimSprite;
	public Vector2 AimSpritePos;
	public bool PortalGunOne;
	public bool PortalGunTwo;
	public bool PortalOne0JustMoved = false;
	public bool PortalOne1JustMoved = true;
	Label Label;
	Label Label2;
	public Timer LockTimer0;
	public Timer LockTimer1;
	[Signal]
	public delegate void OnPortalGunOneActivated(Vector2 Pos);
	[Signal]
	public delegate void OnPortalGunTwoActivated(Vector2 Pos);
	//bool LockedForPortalPair;
 	public bool LockedForPortal0{get; set;}
	public bool LockedForPortal1{get; set;} 

	public bool PortalsEnabled {get; set;} = true;

	public Vector2 CauldronPos {get; set;}

	private AudioStreamPlayer2D _soundPlayerWalk;
	private AudioStreamPlayer2D _soundPlayerSwitchGun;
	private AudioStreamPlayer2D _soundPlayerEffects;
	private Dictionary<AudioData.PlayerSound, AudioStream> _playerSounds = AudioData.LoadedSounds<AudioData.PlayerSound>(AudioData.PlayerSoundPaths);

	public override void _Ready()
	{
		CollisionShape2D = (CollisionShape2D)GetNode("CollisionShape2D");
		Anim = (AnimationPlayer)GetNode("Anim");
		Sprite = (Sprite)GetNode("Sprite");
		ChangeState(State.Idle); //Start as IDLE
		AimSprite = GetNode<Sprite>("AimSprite");
		LockTimer0 = GetNode<Timer>("LockTimer0"); 
		LockTimer1 = GetNode<Timer>("LockTimer1");
		_soundPlayerWalk = GetNode<AudioStreamPlayer2D>("SoundPlayerWalk");
		_soundPlayerSwitchGun = GetNode<AudioStreamPlayer2D>("SoundPlayerSwitchGun");
		_soundPlayerEffects = GetNode<AudioStreamPlayer2D>("SoundPlayerEffects");

	}

	public void PlayWalkSound()
	{
		if (_soundPlayerWalk.Playing)
		{
			return;
		}
		AudioHandler.PlaySound(_soundPlayerWalk, _playerSounds[AudioData.PlayerSound.PlayerWalk], AudioData.SoundBus.Effects);
	}
	public void StopPlayerWalkSound()
	{
		_soundPlayerWalk.Stop();
	}
	public void PlaySwitchGunSound()
	{
		AudioHandler.PlaySound(_soundPlayerSwitchGun, _playerSounds[AudioData.PlayerSound.SwitchPortalGun], AudioData.SoundBus.Effects);
	}
	public void PlaySoundEffect(AudioData.PlayerSound sound)
	{
		AudioHandler.PlaySound(_soundPlayerEffects, _playerSounds[sound], AudioData.SoundBus.Effects);
	}

	public void StartLockTimer0()
	{
		LockTimer0.Start();
	}

	public void StartLockTimer1()
	{
		LockTimer1.Start();
	}

	public bool LockedFor(int _portalPairID)
	{
		if (_portalPairID == 0)
		{
			return LockedForPortal0;
		} 
		if (_portalPairID == 1)
		{
			return LockedForPortal1;
		}
		return false;
	}

	private void OnLockTimer0Timeout()
	{
		LockedForPortal0 = false;
	}

	private void OnLockTimer1Timeout()
	{
		LockedForPortal1 = false;
	}

	public enum State //enum defines a list of variables. //Kept portal and died just in case...
	{
		Idle,
		Moving,
		Portal,
		Died
	};

	public void ChangeState(State state) //Access these from the state machine
	{
		if (PlayerActionState != null)
		{
		}
		switch(state) 
		{
			case State.Idle:
				PlayerActionState = new PlayerIdleActionState(this); //change these after you make script
				break;
 			case State.Moving:
				PlayerActionState = new PlayerMovingActionState(this);
				break;
			case State.Portal:
				PlayerActionState = new PlayerPortalActionState(this);
				break;
			case State.Died:
				PlayerActionState = new PlayerDiedActionState(this);
				break; 
		}
	}

	public State GetState() //access this from the state machine
	{
		if (PlayerActionState is PlayerIdleActionState)
		{
			return State.Idle;
		}
		else if (PlayerActionState is PlayerMovingActionState)
		{
			return State.Moving;
		}
		else
		{
			return State.Died;
		}
	}

	public void FlipLeft(bool flip)
	{
		Sprite.FlipH = flip;
	}

	public void PlayAnim(string anim)
	{
		if (Anim.CurrentAnimation == anim)
		{
			return;
		}
		Anim.Play(anim);
	}

	public override void _PhysicsProcess(float delta)
	{
		GetInput(); //detect user input incl portals
		PlayerActionState.Update(delta); //Keep checking the state machine
		for(int i = 0; i < GetSlideCount(); i++)
		{
			var collision = GetSlideCollision(i);
			if (collision.Collider is PushableObstacle obstacle)
			{
				Vector2 dir = -collision.Normal;
				if (Math.Abs(dir.x) > Math.Abs(dir.y))
				{
					if (dir.x > 0)
					{
						dir = new Vector2(1,0);
					}
					else if (dir.x < -0)
					{
						dir = new Vector2(-1, 0);
					}
				}
				else
				{
					if (dir.y > 0)
					{
						dir = new Vector2(0,1);
					}
					else if (dir.y < -0)
					{
						dir = new Vector2(0, -1);
					}
				}
				obstacle.OnPushed(dir * _pushPower);		
			}
		}
	}

	public void GetInput() 
	{
		MoveDir = new Vector2();
		if ((Input.IsActionPressed("Down") && (Input.IsActionPressed("Left"))))
		{
			MoveDir.y = 1;
			MoveDir.x = -1;
		}
		if (Input.IsActionPressed("Up"))
		{
			MoveDir.y = -1;
		}
		if (Input.IsActionPressed("Down"))
		{
			MoveDir.y = 1;
		}
		if (Input.IsActionPressed("Left"))
		{
			MoveDir.x = -1;
		}
		if (Input.IsActionPressed("Right"))
		{
			MoveDir.x = 1;
		}
		if (Input.IsActionJustPressed("ToggleAim")) //toggle aim
		{  
			if (AimSprite.Visible == false)
			{
				AimSprite.Visible = true;
				GD.Print("ToggleAim: ON");
			}
			else
			{
				AimSprite.Visible = false;
				GD.Print("ToggleAim: OFF");
			}
		}
		if (Input.IsActionJustPressed("EquipPortalGunOne") && PortalsEnabled && !PortalGunOne)
		{
			EquipPortalGunOne();
			EmitSignal(nameof(EquippedPortalGun), 1);
		}
		if (Input.IsActionJustPressed("EquipPortalGunTwo") && PortalsEnabled && !PortalGunTwo)
		{
			EquipPortalGunTwo();
			EmitSignal(nameof(EquippedPortalGun), 2);
		}
		if (Input.IsActionJustPressed("Make Portal") && !PortalsEnabled)
		{
			return;
		}
		if (Input.IsActionJustPressed("Make Portal") && PortalGunOne == true)
		{
			if (!IsCloseToCauldron())
			{
				AimSpritePos = AimSprite.GlobalPosition + Offset;
				EmitSignal(nameof(OnPortalGunOneActivated), AimSpritePos); 
				PlaySoundEffect(AudioData.PlayerSound.EvilPortalGen);
			}
			else
			{
				EmitSignal(nameof(PlayerHint), "Too close to Mother's mouth!");
				PlaySoundEffect(AudioData.PlayerSound.Fizzle);
			}

		}
		if (Input.IsActionJustPressed("Make Portal") && PortalGunTwo == true)
		{
			if (!IsCloseToCauldron())
			{
				AimSpritePos = AimSprite.GlobalPosition + Offset;
				EmitSignal(nameof(OnPortalGunTwoActivated), AimSpritePos); 
				PlaySoundEffect(AudioData.PlayerSound.EvilPortalGen);
			}
			else
			{
				EmitSignal(nameof(PlayerHint), "Too close to Mother's mouth!");
				PlaySoundEffect(AudioData.PlayerSound.Fizzle);
			}
		}  
		AimSprite.RotationDegrees = RotationDegree - 90; 	
	}

	public void EquipPortalGunOne()
	{
		EmitSignal(nameof(PlayerHint), "Equipping Portal Device Alpha");
		PortalGunOne = true;
		PortalGunTwo = false;
		PlaySwitchGunSound();
	}
	public void EquipPortalGunTwo()
	{
		EmitSignal(nameof(PlayerHint), "Equipping Portal Device Beta");
		PortalGunOne=false;
		PortalGunTwo=true;
		PlaySwitchGunSound();
	}

	public bool IsCloseToCauldron()
	{
		return GlobalPosition.DistanceTo(CauldronPos) < 500;
	}

	[Signal]
	public delegate void PlayerHint(string text);

	[Signal]
	public delegate void EquippedPortalGun(int num);

}
