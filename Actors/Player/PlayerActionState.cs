using Godot;
using System;

public class PlayerActionState : Node
{
    public Player Player;
    public Player.State NextState;
    public virtual void Update(float delta)
    {

    }

    public virtual void _Process(float delta)
    {

    }

}
