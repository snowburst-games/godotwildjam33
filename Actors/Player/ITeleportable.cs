using Godot;
using System;

public interface ITeleportable
{
    bool LockedForPortal0{get; set;}
    bool LockedForPortal1{get; set;}
    bool LockedFor (int _portalPairID);
    void StartLockTimer0();
    void StartLockTimer1();
} 


