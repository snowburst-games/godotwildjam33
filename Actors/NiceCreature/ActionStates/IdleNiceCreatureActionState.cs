using Godot;
using System;

public class IdleNiceCreatureActionState : NiceCreatureActionState
{
	public IdleNiceCreatureActionState()
	{

	}

	public IdleNiceCreatureActionState(NiceCreature niceCreature)
	{
		this.NiceCreature = niceCreature;
	}

	public override void Update(float delta)
	{
		base.Update(delta);

		// currently we don't use an idle state...
		if (NiceCreature.Dying)
		{
			return;
		}
		NiceCreature.SetActionState(NiceCreature.ActionState.Moving);

	}
}
