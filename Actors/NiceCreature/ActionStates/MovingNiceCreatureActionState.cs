using Godot;
using System;

public class MovingNiceCreatureActionState : NiceCreatureActionState
{
	public MovingNiceCreatureActionState()
	{

	}

	public MovingNiceCreatureActionState(NiceCreature niceCreature)
	{
		this.NiceCreature = niceCreature;
	}

	

	public override void Update(float delta)
	{
		base.Update(delta);
		GD.Print(NiceCreature.MoveDirection);
		NiceCreature.PlayAnimMove(NiceCreature.MoveDirection == new Vector2(1,0) ? "Right" : 
			NiceCreature.MoveDirection == new Vector2(0,1) ? "Down" :
			NiceCreature.MoveDirection == new Vector2(-1,0) ? "Left" :
			"Up");
		NiceCreature.MoveAndSlide(NiceCreature.MoveDirection * NiceCreature.Speed, infiniteInertia:false);
		if (NiceCreature.Dying)
		{
			NiceCreature.SetActionState(NiceCreature.ActionState.Idle);
		}
		// NiceCreature.Position += (NiceCreature.MoveDirection * NiceCreature.Speed * delta);

		// anim depending on direction
	}
}
