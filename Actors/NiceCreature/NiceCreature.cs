using Godot;
using System;
using System.Collections.Generic;

public class NiceCreature : KinematicBody2D, ITeleportable
{
	private AudioStreamPlayer2D _soundPlayer;
	private AnimationPlayer _animMove;
	private AnimationPlayer _animSpawnFade;
	private Dictionary<AudioData.NiceCreatureSound, AudioStream> _niceCreatureSounds = AudioData.LoadedSounds<AudioData.NiceCreatureSound>(AudioData.NiceCreatureSoundPaths);
	// ITeleportable code
	public bool LockedForPortal0 {get; set;}
	public bool LockedForPortal1 {get; set;}

	public void StartLockTimer0()
	{
		GetNode<Timer>("LockTimer1").Start();
	}
	public void StartLockTimer1()
	{
		GetNode<Timer>("LockTimer2").Start();
	}

	public bool LockedFor(int _portalPairID)
	{
		return _portalPairID == 0 ? LockedForPortal0 : LockedForPortal1;
	}

	private void OnLockTimer1Timeout()
	{
		LockedForPortal0 = false;
	}

	private void OnLockTimer2Timeout()
	{
		LockedForPortal1 = false;	
	}

	// ITeleportable code ends

	[Signal]
	public delegate void Escaped();

	[Signal]
	public delegate void Evilled();

	public enum ActionState {Idle, Moving}

	private NiceCreatureActionState _currentActionState;
	private Timer _collisionCooldown;

	// public enum Direction {Up, Right, Down, Left}

	// public Direction CurrentDirection {get; set;} = Direction.Up; // Set this from a generator
	public Vector2 MoveDirection {get; set;} = new Vector2(0, -1); // 0, -1 / 1, 0 / 0, 1 / -1, 0
	public float Speed {get; set;} = 300;
	private Random _rand = new Random();
	public override void _Ready()
	{
		_soundPlayer = GetNode<AudioStreamPlayer2D>("SoundPlayer");
		_animMove = GetNode<AnimationPlayer>("AnimMove");
		_animSpawnFade = GetNode<AnimationPlayer>("AnimSpawnFade");
		_collisionCooldown = GetNode<Timer>("CollisionCooldown");
		GetNode<Sprite>("Sprite").Scale = new Vector2(0,0);
		Start();
	}

	public async void Start()
	{
		_animSpawnFade.Play("Spawn");
		await ToSignal(_animSpawnFade, "animation_finished");
		SetActionState(ActionState.Idle);
	}

	public void SetActionState(ActionState state)
	{
		switch (state)
		{
			case ActionState.Idle:
				_currentActionState = new IdleNiceCreatureActionState(this);
				break;
			case ActionState.Moving:
				_currentActionState = new MovingNiceCreatureActionState(this);
				break;
		}
	}

	public AudioData.NiceCreatureSound GetRandomHappySound()
	{
		AudioData.NiceCreatureSound[] sounds = new AudioData.NiceCreatureSound[5] 
			{AudioData.NiceCreatureSound.FoundNice2, AudioData.NiceCreatureSound.Freedom, AudioData.NiceCreatureSound.NiceFound1, 
			AudioData.NiceCreatureSound.Woohoo1, AudioData.NiceCreatureSound.Yay};
		return sounds[_rand.Next(sounds.Length)];
	}
	public AudioData.NiceCreatureSound GetRandomSadSound()
	{
		AudioData.NiceCreatureSound[] sounds = new AudioData.NiceCreatureSound[3] 
			{AudioData.NiceCreatureSound.Ooh, AudioData.NiceCreatureSound.Aah, AudioData.NiceCreatureSound.Ow};
		return sounds[_rand.Next(sounds.Length)];
	}

	public override void _PhysicsProcess(float delta)
	{
		base._PhysicsProcess(delta);

		if (_currentActionState == null)
		{
			return;
		}
		_currentActionState.Update(delta);

		HandleCollisions();

		if (Dying)
		{
			float x = Mathf.Lerp(Position.x, Position.x + (MoveDirection.x * 256 * delta), (_animSpawnFade.CurrentAnimationPosition/_animSpawnFade.CurrentAnimationLength));
			float y = Mathf.Lerp(Position.y, Position.y + (MoveDirection.y * 256 * delta), (_animSpawnFade.CurrentAnimationPosition/_animSpawnFade.CurrentAnimationLength));
			Position = new Vector2(x, y);
		}
	}
	// private Vector2 _dyingDirection = new Vector2();

	public void PlayAnimMove(string anim)
	{
		if (_animSpawnFade.IsPlaying())
		{
			return;
		}
		if (_animMove.IsPlaying() && _animMove.CurrentAnimation == anim)
		{
			return;
		}
		_animMove.Play(anim);
	}

	private void HandleCollisions()
	{
		for (int i = 0; i < GetSlideCount(); i++)
		{
			var collision = GetSlideCollision(i);

			if (collision.Collider is Cauldron cauldron)
			{
				EvilEnd();
				cauldron.OnNiceCreatureCollided();
			}
			if (collision.Collider is StaticBody2D staticbody || collision.Collider is TileMap tileMap || collision.Collider is PushableObstacle pushableObstacle)
			{
				// if (!(collision.Collider is Cauldron))
				// {
					Turn();
				// }
			}			
			if (collision.Collider is Player player) 
			{
				// Escape();
			}
		}
	}

	public void Turn(bool cooldown = true)
	{
		if (Dying)
		{
			return;
		}
		if (_collisionCooldown.TimeLeft > 0 && cooldown)
		{
			return;
		}
		MoveDirection = MoveDirection.Rotated(Mathf.Pi/2f);
		MoveDirection = new Vector2(Mathf.Round(MoveDirection.x), Mathf.Round(MoveDirection.y));
		_collisionCooldown.Start();
	}

	public bool Dying {get; set;} = false;

	// This runs when the creature collides with a nice portal
	// (and also when player collides with it?)
	public async void Escape()
	{
		if (Dying)
		{
			return;
		}
		// _dyingDirection = MoveDirection;
		Dying = true;
		Speed = 0;
		_animSpawnFade.Play("Escape");
		AudioHandler.PlaySound(_soundPlayer, _niceCreatureSounds[GetRandomHappySound()], AudioData.SoundBus.Voice);
		await ToSignal(_animSpawnFade, "animation_finished");
		if (_soundPlayer.Playing)
		{
			await ToSignal(_soundPlayer, "finished");
		}
		EmitSignal(nameof(Escaped));
		QueueFree();
	}

	public async void EvilEnd()
	{
		if (Dying)
		{
			return;
		}
		// _dyingDirection = MoveDirection;
		Dying = true;
		Speed = 0;
		_animSpawnFade.Play("EvilEnd");
		AudioHandler.PlaySound(_soundPlayer, _niceCreatureSounds[GetRandomSadSound()], AudioData.SoundBus.Voice);
		await ToSignal(_animSpawnFade, "animation_finished");
		if (_soundPlayer.Playing)
		{
			await ToSignal(_soundPlayer, "finished");
		}
		EmitSignal(nameof(Evilled));
		QueueFree();
	}


	public void OnTeleport(Vector2 pos)
	{
		this.Position = pos; // update when sarah fixes her code
	}
}
