using Godot;
using System;
using System.Collections.Generic;

public class PushableObstacle : RigidBody2D
{
	// private float _speed = 650;

	private AudioStreamPlayer2D _soundPlayer;
	private Sprite _sprite;
	private Random _rand = new Random();
	private Dictionary<AudioData.ObstaclePushSound, AudioStream> _obstaclePushSounds = AudioData.LoadedSounds<AudioData.ObstaclePushSound>(AudioData.ObstaclePushSoundPaths);
	// public float Speed {get; set;} = 25000;
	// private Vector2 _pushForce {get; set;}
	// private Vector2 _pushDir;
	// private Area2D _detectArea;

	private List<Texture> _textures = new List<Texture>() {
		GD.Load<Texture>("res://Props/PushableObstacle/Art/blue gumball v0.1.png"),
		GD.Load<Texture>("res://Props/PushableObstacle/Art/gold gumball v0.3.png"),
		GD.Load<Texture>("res://Props/PushableObstacle/Art/green gumball v0.2.png"),
		GD.Load<Texture>("res://Props/PushableObstacle/Art/red gumball v0.4.png"),
		GD.Load<Texture>("res://Props/PushableObstacle/Art/yellow gumball v0.4.png")
	};

	public override void _Ready()
	{
		_soundPlayer = GetNode<AudioStreamPlayer2D>("SoundPlayer");
		_sprite = GetNode<Sprite>("Sprite");
		_sprite.Texture = _textures[_rand.Next(0, _textures.Count)];
		
		// _detectArea = GetNode<Area2D>("DetectArea");
	}
// for index in get_slide_count():
//         var collision = get_slide_collision(index)
//             if collision.collider.is_in_group("bodies"):
//                 collision.collider.apply_central_impulse(-collision.normal * push)
	public override void _PhysicsProcess(float delta)
	{
		base._PhysicsProcess(delta);

		// foreach (PhysicsBody2D body in GetCollidingBodies())
		// {
		// 	GD.Print(body);
		// 	GD.Print("test");
		// }

		// GD.Print(GetCollidingBodies());

		// CalcCollisions(MoveAndCollide(
		// 	(_pushForce * Speed) * delta
		// 	, false));

		// GD.Print(_pushForce);

		// _pushForce = new Vector2(0,0);
	}

	public void OnPushed(Vector2 power)
	{
		ApplyCentralImpulse(power);
	}

	public override void _IntegrateForces(Physics2DDirectBodyState state)
	{
		base._IntegrateForces(state);

		RotationDegrees = 0;
		if (LinearVelocity.Length() > 100)
		{
			if (!_soundPlayer.Playing)
			{
				AudioHandler.PlaySound(_soundPlayer, _obstaclePushSounds[AudioData.ObstaclePushSound.Pushed], AudioData.SoundBus.Effects);
			}
		}
		// GD.Print(LinearVelocity.Length());
	}


	private void OnCreatureAreaBodyEntered(Godot.Object body)
	{
		if (body is NiceCreature niceCreature)
		{
			niceCreature.Turn(false);
		}
	}

	// private void OnPushableObstacleBodyEntered(PhysicsBody2D body)
	// {

	// }


	// public void OnGetPushed(Vector2 fromDir)
	// {
	// 	_pushDir = fromDir;
	// 	_pushForce = Speed * fromDir;
	// }


	// public void CalcCollisions(KinematicCollision2D collisionData)
	// {
	// 	if (collisionData == null)
	// 	{
	// 		_pushForce = new Vector2(0,0);
	// 		return;
	// 	}

	// 	if (collisionData.Collider is PlayerTest playerTest)
	// 	{
	// 		Vector2 dir = Position - playerTest.Position;
	// 		OnGetPushed(dir.Normalized());

	// 	}

	// 	if (collisionData.Collider is PushableObstacle obstacle)
	// 	{
	// 		if (_pushForce != new Vector2(0,0))
	// 		{
	// 			obstacle.OnGetPushed(_pushDir);
	// 		}

	// 	}
	// }
	// private void OnDetectAreaBodyEntered(Godot.Object body)
	// {
	// 	// Replace with function body.
	

	// private void OnPushableObstacleBodyEntered(PhysicsBody2D body)
	// {
		
	// }


	// public void OnGetPushed(Vector2 fromDir)
	// {
	// 	_pushDir = fromDir;
	// 	_pushForce = Speed * fromDir;
	// }


	// public void CalcCollisions(KinematicCollision2D collisionData)
	// {
	// 	if (collisionData == null)
	// 	{
	// 		_pushForce = new Vector2(0,0);
	// 		return;
	// 	}

	// 	if (collisionData.Collider is PlayerTest playerTest)
	// 	{
	// 		Vector2 dir = Position - playerTest.Position;
	// 		OnGetPushed(dir.Normalized());
			
	// 	}

	// 	if (collisionData.Collider is PushableObstacle obstacle)
	// 	{
	// 		if (_pushForce != new Vector2(0,0))
	// 		{
	// 			obstacle.OnGetPushed(_pushDir);
	// 		}

	// 	}
	// }
	// private void OnDetectAreaBodyEntered(Godot.Object body)
	// {
	// 	// Replace with function body.
	// }
}

