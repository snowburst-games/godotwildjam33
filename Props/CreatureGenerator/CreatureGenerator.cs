using Godot;
using System;

public class CreatureGenerator : StaticBody2D
{

	[Signal]
	public delegate void CreatureGenerated(NiceCreature creature);

	private PackedScene _creatureScn;

	private Timer _timer;

	[Export]
	private int _maxCreatures = 20; // per generator

	private int _creatureCount = 0;

	[Export]
	private Vector2 _startingDir = new Vector2(0,1);

	[Export]
	private float _timeBetween = 1f;

	public override void _Ready()
	{
		base._Ready();
		_creatureScn = GD.Load<PackedScene>("res://Actors/NiceCreature/NiceCreature.tscn");
		_timer = GetNode<Timer>("Timer");

		_timer.WaitTime = Math.Max(_timeBetween, 0.5f);
		_timer.Start();
	}

	private void OnTimerTimeout()
	{
		if (_creatureCount >= _maxCreatures)
		{
			return;
		}
		NiceCreature _creature = (NiceCreature) _creatureScn.Instance();
		_creature.MoveDirection = _startingDir;
		_creature.Position += ((GetNode<Sprite>("Sprite").Texture.GetSize()*0.3f) *_startingDir);
		AddChild(_creature);
		EmitSignal(nameof(CreatureGenerated), _creature);
		_creatureCount += 1;
		_timer.Start();
	}

	public void KillCreatures()
	{
		foreach (Node n in GetChildren())
		{
			if (n is NiceCreature creature)
			{
				creature.QueueFree();
			}
		}
	}

}

