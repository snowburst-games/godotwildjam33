using Godot;
using System;

public class Portal : Area2D
{
	public int Id; //label one portal 0 and one portal 1 so there are a pair of portals under each portal manager, each with different ids
	[Export]
	public int PortalPairID {get; set;} //All the portals under PortalManager1 are PortalPairID = 0; all portals under PortalManager2 are PortalPairID = 1.

	private Random _rand = new Random();
	[Signal]
	public delegate void OnPortalActivated(int id, KinematicBody2D body);

	public override void _Ready()
	{
		GetNode<Sprite>("Sprite").FlipH = (_rand.Next(0,2) == 0);
		if (PortalPairID == 0)
		{
			GetNode<CPUParticles2D>("CPUParticles2D0").Visible = true;
			GetNode<CPUParticles2D>("CPUParticles2D1").Visible = false;
		}
		else
		{
			GetNode<CPUParticles2D>("CPUParticles2D0").Visible = false;
			GetNode<CPUParticles2D>("CPUParticles2D1").Visible = true;
		}
	}

	private void OnPortalBodyEntered(KinematicBody2D body)
	{
		if (body is ITeleportable t)
		{
			if (!t.LockedFor(PortalPairID))
			{
				int _portalID = this.Id; //need this to figure out the location of the paired portal
				if (PortalPairID == 0)
				{
					t.StartLockTimer0();
					// GD.Print("PortalPairIDzer0" + PortalPairID);
					t.LockedForPortal0 = true;
				} 
				if (PortalPairID == 1)
				{
				   t.StartLockTimer1();
				//    GD.Print("PortalPairIDOne" + PortalPairID);
				   t.LockedForPortal1 = true;
				}  
				EmitSignal(nameof(OnPortalActivated), _portalID, t); //connect this to PortalManager
			}
		}
   
	}
}



