using Godot;
using System;
using System.Collections.Generic;

public class PortalManager : Node2D
{
	int _portalEntered;
	public Portal Portal;
	bool _lockPortal = false;
	public Vector2 _portalId0Pos = new Vector2 (100,300);
	public Vector2 _portalId1Pos  = new Vector2 (200,100);
	Portal PortalZero;
	Portal PortalOne;
	int PortalId0 = 0;
	int PortalId1 = 1;   
	[Signal]
	public delegate void OnTeleport(); //remove this
	public Portal Portal0;
	public Portal Portal1;
	public int NumberOfPortalsActivated = 0;
	public Portal[] Portals {get; set;}
	public CollisionShape2D Portal0Collision;
	public CollisionShape2D Portal1Collision;
	List <Portal> PortalListOne = new List<Portal>();
	List <Portal> PortalListTwo = new List<Portal>();
	Portal ActivePortal;
	[Export]
	int PortalPairID0;
	[Export]
	int PortalPairID1;

	private Random _rand = new Random();

	public override void _Ready()
	{
		Portals = new Portal[2] {GetNode<Portal>("Portal"),GetNode<Portal>("Portal2")}; 
		PortalZero = GetNode<Portal>("Portal");
		PortalOne = GetNode<Portal>("Portal2");   
		PortalZero.Id = PortalId0;
		PortalOne.Id = PortalId1;
		Portal0 = GetNode<Portal>("Portal");
		Portal1 = GetNode<Portal>("Portal2");
		PortalPairID0 = Portal0.PortalPairID;
		PortalPairID1 = Portal1.PortalPairID;
		Portal0Collision = (CollisionShape2D)GetNode("Portal/CollisionShape2D");
		Portal1Collision = (CollisionShape2D)GetNode("Portal2/CollisionShape2D");
 		Portal0Collision.Disabled = true;
		Portal0Collision.Disabled = true;
		PortalListOne.Add(Portal0);
		PortalListOne.Add(Portal1);
		PortalListTwo.Add(Portal0);
		PortalListTwo.Add(Portal1);
		MakePortalInvisible();
		
	}

	public void MakePortalInvisible()
	{
		Portal0.Visible = false;
		Portal1.Visible = false;

	}

	private void OnPortalActivated(int id, KinematicBody2D t) //sets body to the location of the paired portal
	{
		_portalEntered = id;
		foreach (Portal portal in Portals)
		{
			if (portal.Id != _portalEntered)
			{
				t.GlobalPosition = portal.GlobalPosition;
				if (t is Player player)
				{
					player.PlaySoundEffect(_rand.Next(0,2) == 0 ? AudioData.PlayerSound.EvilPortalTravel1 : AudioData.PlayerSound.EvilPortalTravel2);
				}
				//EmitSignal(nameof(OnTeleport));  //REMOVE ON TELEPORT
			}
		}
	}

	private void OnPortalGunOneActivated(Vector2 Pos)
	{
		ActivePortal = PortalListOne[0];
		ActivePortal.Visible = true;
		ActivePortal.GlobalPosition = Pos;
		PortalListOne.Remove(ActivePortal);
		PortalListOne.Add(ActivePortal);
		NumberOfPortalsActivated ++;
		if (NumberOfPortalsActivated>1)
		{
			//enable both collision shapes when both portals have been activated
			Portal0Collision.Disabled = false;
			Portal1Collision.Disabled = false;
		} 

	}
	private void OnPortalGunTwoActivated(Vector2 Pos)
	{
			ActivePortal = PortalListTwo[0];
			ActivePortal.Visible = true;
			ActivePortal.GlobalPosition = Pos;
			PortalListTwo.Remove(ActivePortal);
			PortalListTwo.Add(ActivePortal);
			NumberOfPortalsActivated ++;
			if (NumberOfPortalsActivated>1)
			{
				//enable both collision shapes when both portals have been activated
				Portal0Collision.Disabled = false;
				Portal1Collision.Disabled = false;
			}
	}
	
}   
