using Godot;
using System;
using System.Collections.Generic;

public class NicePortal : Area2D
{
	private AudioStreamPlayer2D _soundPlayer;
	private Dictionary<AudioData.GoodPortalSound, AudioStream> _portalSounds = AudioData.LoadedSounds<AudioData.GoodPortalSound>(AudioData.GoodPortalSoundPaths);
	private AnimationPlayer _anim;
	private Random _rand = new Random();
	public override void _Ready()
	{
		_soundPlayer = GetNode<AudioStreamPlayer2D>("SoundPlayer");
		_anim = GetNode<AnimationPlayer>("Anim");
		GetNode<Sprite>("Sprite").FlipH = (_rand.Next(0,2) == 0);
	}

	private void OnNicePortalBodyEntered(PhysicsBody2D body)
	{
		if (body is NiceCreature niceCreature)
		{
			niceCreature.Escape();
			PlaySound(AudioData.GoodPortalSound.Escape);
		}
	}

	private void PlaySound(AudioData.GoodPortalSound sound)
	{
		if (_soundPlayer.Playing)
		{
			return;
		}
		AudioHandler.PlaySound(_soundPlayer, _portalSounds[sound], AudioData.SoundBus.Effects);
	}

	public void Spawn()
	{
		_anim.Play("Appear");
		PlaySound(AudioData.GoodPortalSound.Gen);
	}

	public async void Die()
	{
		_anim.Play("Fade");
		PlaySound(AudioData.GoodPortalSound.Fade);
		await ToSignal(_anim, "animation_finished");
		// do die animation
		if (_soundPlayer.Playing)
		{
			await ToSignal (_soundPlayer, "finished");
		}
		QueueFree();
	}

}


