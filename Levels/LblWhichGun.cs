using Godot;
using System;

public class LblWhichGun : Label
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        string equipPortalGunOne = ((InputEvent)InputMap.GetActionList("EquipPortalGunOne")[0]).AsText();
        string equipPortalGunTwo = ((InputEvent)InputMap.GetActionList("EquipPortalGunTwo")[0]).AsText();


        Text = String.Format("I can switch between portal devices by pressing {0} or {1}.",
            equipPortalGunOne, equipPortalGunTwo);
    }

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
}
