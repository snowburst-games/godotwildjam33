using Godot;
using System;

public class LblPortalActivateIntro : Label
{
    // Declare member variables here. Examples:
    // private int a = 2;
    // private string b = "text";

    // Called when the node enters the scene tree for the first time.
    public override void _Ready()
    {
        string makePortal = ((InputEvent)InputMap.GetActionList("Make Portal")[0]).AsText();
        string equipPortalGunOne = ((InputEvent)InputMap.GetActionList("EquipPortalGunOne")[0]).AsText();
        string equipPortalGunTwo = ((InputEvent)InputMap.GetActionList("EquipPortalGunTwo")[0]).AsText();


        Text = String.Format("I have two portal devices. Equip one at a time.\nWhen equipped, press {0} to place the portal entrance and press {0} again to place the portal exit. You can do this as many times as you wish.",
            makePortal);

    }

//  // Called every frame. 'delta' is the elapsed time since the previous frame.
//  public override void _Process(float delta)
//  {
//      
//  }
}
