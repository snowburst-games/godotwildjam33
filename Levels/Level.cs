using Godot;
using System;

public class Level : Node2D
{
	[Export]
	public int TargetEvil {get; set;} = 20;
	
	[Export]
	public int EscapeAvoid {get; set;} = 5;
	
	[Export]
	public string LevelName {get; set;} = "NoName";

	[Export]
	public StageWorld.LevelWorld NextLevel {get; set;}

	[Export]
	public string EndLevelText {get; set;} = "";

	[Export]
	public string EndLevelPicturePath {get; set;}

	[Export]
	public AudioData.WorldSound AudioPath {get; set;}
}
