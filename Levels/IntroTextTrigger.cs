using Godot;
using System;

public class IntroTextTrigger : Area2D
{
private bool _alreadyActivated = false;
AnimationPlayer Anim;
[Export]
private string _animName;

	public override void _Ready()
	{
		Anim = GetNode<AnimationPlayer>("Anim");
	}

	private void OnIntroTextTriggerBodyEntered(Godot.Object body)
	{
		if (body is Player player && ! _alreadyActivated)
		{
			Anim.Play(_animName);
			_alreadyActivated = true;
		}
	}

}
